<?php 
header ("P3P:CP=\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"");

session_start(); // sessie beginnen

function cleanup($string="") { 
	$string = strip_tags($string); 
	$string = htmlspecialchars($string); 
	if(!get_magic_quotes_gpc()) { 
	  $string = addslashes($string); 
	} 
	return $string; 
} 

// controleren of pagina correct is aangeroepen.

if (!isset($_POST['klas']) AND !isset($_GET['cl'])) { 
	 	$tekst = "<font face=\"verdana\" size=\"4\">Pagina ongeldig aangeroepen!<br>Probeer opnieuw: 
	 	<a href=\"klas.htm\"\" onmouseover=\"window.status='';return true\"></a></font><br>";
	 	die($tekst);
	 
}else{
				
	 	//  $klas = cleanup($_POST["klas"]);	
		
	 	include("inc_connect.php");
	 	$klas = cleanup($_POST["klas"]); 
		
		if (isset($_GET['cl'])) {
			 $klas = cleanup($_GET['cl']);
		}
		
		if ($klas=="A2a")
			 { $klas = "G2a";
		}
			
		date_default_timezone_set('Europe/Amsterdam');
		setlocale(LC_ALL, 'nl_NL');

		$vandaag = strftime("%Y-%m-%d", mktime(date("j F Y")));
		$week		 = intval(strftime("%W", strtotime($vandaag)));
		
		//  id ophalen van huidige week
		
		$query = "SELECT * FROM weken WHERE week = '$week'";
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$weken = array();
		while($weken[] = mysql_fetch_array($result)); // data uit tabel:weken in $week[index][velden]
		$weekid = $weken[0][0];  // eventueel + getal achter [0][0] voor testen aantal weken verder
				
		// ophalen van het toetsrooster van de klas voor de 4 eerstvolgende weken
									
		$optehalenweek = 42;  //  er moet een geldig rooster van deze week in de tabel staan
		
		$query = "SELECT * FROM rooster WHERE week = '$optehalenweek' AND klas = '$klas' ORDER BY id";
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$aantallessen = mysql_num_rows($result);
				
		if (mysql_num_rows($result) > 0) {
		
			 $rooster= array();
			 while($rooster[] = mysql_fetch_array($result)); // data uit tabel:date in $klasrooster[index][velden]
		}
					
		$eersteweekid = $weekid;
		$laatsteweekid = $weekid + 3;
		
		$query = "SELECT * FROM toetsopgaven WHERE weekid >= '$eersteweekid' AND weekid <= '$laatsteweekid' And klas = '$klas' ORDER BY week";  // opgegeven toetsen ophalen
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
		$aantaltoetsen = mysql_num_rows($result);
	
		if (mysql_num_rows($result) > 0) {
		
			 $toetsen= array();
			 while($toetsen[] = mysql_fetch_array($result)); // data uit tabel:date in $toetsen[index][velden]
				
		}	
		
		//  echo $toetsen[0][2] . "gewicht:" . $toetsen[0][5] . "les:" . $toetsen[0][6] . "week:" . $toetsen[0][7] . "<br>";
		//  echo $toetsen[1][2] . "gewicht:" . $toetsen[1][5] . "les:" . $toetsen[1][6] . "week:" . $toetsen[1][7] . "<br>";
		//  echo $toetsen[2][2] . "gewicht:" . $toetsen[2][5] . "les:" . $toetsen[2][6] . "week:" . $toetsen[2][7] . "<br>";
		
		$query = "SELECT * FROM weken WHERE id >= '$weekid' ORDER BY id LIMIT 0,4";  // weekinfo ophalen
		$result = mysql_query($query) or die ("FOUT: " . mysql_error());
			
		if (mysql_num_rows($result) > 0) {
		
			 $data= array();
			 while($data[] = mysql_fetch_array($result)); // data uit tabel:date in $data[index][velden]
				
		}
		
		for ($t=0; $t<4; $t++)  {  //  lesrooster overnemen voor volgende 3 weken
				$klasrooster[$t] = $rooster;
		}	
				
		//  opgehaalde toetsen in rooster plaatsen
		
		for ($t=0; $t<$aantaltoetsen; $t++)  {  //  maak lvdw (les van de week) allemaal 0
				$lvdw[$t] = 0;
		}				 
		
		for ($w=0; $w<4; $w++) {  // 4 weken ophalen te beginnen met deze week en toetsen in rooster plaatsen 
				
				$wk = $data[$w][1];  //  weeknummer lezen
				
				for ($les=0; $les<$aantallessen; $les++) {  //  alle lessen van de klas van deze week aflopen
				
						$rd = $klasrooster[$w][$les][5];  //  docent uit het rooster
						$rv = $klasrooster[$w][$les][4];  //  vak uit rooster
												
						for ($t=0; $t<$aantaltoetsen; $t++) {  //  alle opgegeven toetsen aflopen
														
								if ($toetsen[$t][7] == ($wk)) {  //  als week klopt
									 if (strtolower($rd) == strtolower($toetsen[$t][2])) {	//  docent uit rooster is docent van toets
									 		//  echo "docent klopt" . ($w + $week) . ":" . $les . ":" . $t  . "<br>";
											//  echo $w . ":" . $klasrooster[$w][$les][5] . "dag:" . $klasrooster[$w][$les][2] . "uur:" . $klasrooster[$w][$les][3];
											//  echo $lvdw[$t];
											if ( (strtolower($rv) == strtolower($toetsen[$t][4])) or ($toetsen[$t][4]=="")) {  //  vak uit rooster is vak toets
									 			 // echo "week en docent klopt";
												 $lvdw[$t] = $lvdw[$t] + 1;
									 			 if (($toetsen[$t][6]) == $lvdw[$t]) {  // juiste les van de week
												 		$klasrooster[$w][$les][7] = $toetsen[$t][5];
														if (!($toetsen[$t][9]=="")) {$klasrooster[$w][$les][8] = $toetsen[$t][9];}  //  opmerking van toets naar rooster
													}
									 		}
									 }
								}
						}  //  einde lus voor aflopen toetsen						
						
				}  //  einde lus voor lessen van de week
					 
		}  //  einde weeklus

}
?>

		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html lang="nl" xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title><?php echo "Toetsrooster klas&nbsp;" . $klas ?></title>
		
		<script language="JavaScript">
		<!-- Begin

		function myprint() {
		window.focus();
		window.print();
		}
		//  End -->
		</script>

		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<style type="text/css"> 
		
		a.tooltip{
		position:relative;
		text-decoration:underline;
		padding:0 0.1em 0 0.1em}
		a.tooltip span{
		padding:0.5em;
		display:none}
		a.tooltip:hover span{
		text-decoration:none;
		display:block;
		position:absolute;
		top:1.5em;
		left:-1.5em;
		width:13em;
		border:1px solid #999999;
		background:#cccccc;
		color:#000000;
		font-size: 0.8em;
		text-align:center}
		
		
		body{
		margin: 0;
		font-family: arial narrow, sans-serif;
		font-size: 0.75em;
		}
 
 		div#header{
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 80px;
		background-color: #e7ebef;
		}
 
 		div#content{
		padding: 10px 10px 0 10px;
		background-color: #e7ebef;
		}
		</style>
		
		<style type="text/css" media="print"> 
		
		#content{
		display: none;
		}

		div.page { 
		writing-mode: tb-rl; 
		width: 100%;
		height: 100%; 
		margin: 10% 0%; 
		}
		</style>

		</head>
		
		<body bgcolor="#e7ebef">
 
 		<div id="content">
			<form method="post" action="klasopvragen.php "alt="">
				<table width="99%"> 
					<tr>
						<td valign="middle" height="15" width="15%">
							<select name="klas" size="1" onchange="this.form.submit()">
								<option><?php echo $klas?></option>
								<option>Mh1a</option>
								<option>M1a</option>
								<option>M1b</option>
								<option>AH1a</option>
								<option>AH1b</option>
								<option>G1a</option>
								<option>Tt1a</option>
								<option>M2a</option>
								<option>M2b</option>
								<option>H2a</option>
								<option>A2a</option>
								<option>G2a</option>
								<option>H3a</option>
								<option>A3a</option>
								<option>G3a</option>
							</select>				
						</td>
						
						<td>
								<center>
										<INPUT TYPE =button name="cmdPrint" 
						 				<?php echo "value = \"Toetsrooster van&nbsp;" . $klas . "&nbsp;afdrukken\" "?>
						 				onClick="myprint()" style="color: navy;" style="text-decoration: none;">
								</center>
						</td>
						
						<td width="25%"></td>
						
						<?if ($klas == "G2a") {
								 $kl = "G2a (en A2a)";
						}else{
								 $kl = $klas;
						}
						?>
						
						<td width="60%" align="right" valign="middle" ><font size="5">Toetsrooster van <?php echo $kl ?></font>
						</td>
						
					</tr>
				</table>
			</form>
		<br>

		</div>	
		
		<fieldset>
	
		<table> 
			<font size="2">
				<tr>
					<td width="5%" height="25"></td>
					<td width="19%" colspan="9"><font size="4">Maandag</font></td>
					<td width="19%" colspan="9"><font size="4">Dinsdag</font></td>
					<td width="19%" colspan="9"><font size="4">Woensdag</font></td>
					<td width="19%" colspan="9"><font size="4">Donderdag</font></td>
					<td width="19%" colspan="9"><font size="4">Vrijdag</font></td>
				</tr>
		
				<tr align="center">
					<td height="8"></td>
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
			
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				
					<td width="2%">1</td><td width="2%">2</td><td width="2%">3</td><td width="2%">4</td>
					<td width="2%">5</td><td width="2%">6</td><td width="2%">7</td><td>8</td><td width="3%"></td>
				</tr>
		
							
				<?php 
			
				$toetsvrij = 0;
				
				for ($w=0; $w<4; $w++) {  // 4 weken ophalen te beginnen met deze week 
					
					$idteller = 0;
					$wk = $data[$w][1];  //  weeknummer lezen
					
					echo "<tr>";
					echo 				"<td colspan=\"45\"><hr></td>";
					echo "</tr>";
					
					$time = strtotime($data[$w][2]);
					
					$aweek = $wk;
										
					echo "<tr>";
					echo 				"<td colspan =\"1\" align=\"left\" height=\"12\"><font size=\"4\"><b>" . $aweek . "</b></font></td><td colspan =\"5\" align=\"left\"><font size=\"2\"><i><u>". strftime('%e %B', $time) . "</u></i></font></td><td colspan =\"18\" ><font size=\"2\" color=\"#ff0000\"><i><b>" . $data[$w][5] . "</i></b></font></td>";
					echo "</tr>";
					
					//  '%a. %e-%m'  oude
					echo "<tr>";
					echo 				"<td colspan=\"45\" height=\"10\"></td>";
					echo "</tr>";
					
					echo "<tr>";
					echo 				"<td align=\"left\" height=\"10\"></td>";
					echo "<font size=\"3\">";
					
					$dag[1] = "Maandag";
					$dag[2] = "Dinsdag";
					$dag[3] = "Woensdag";
					$dag[4] = "Donderdag";
					$dag[5] = "Vrijdag";
					
					for ($d=1; $d<6; $d++) {  // alle dagen van de week aflopen
				
						for ($u=1; $u<9; $u++) {  // alle uren van de dag aflopen
							// echo $d . ":" . $klasrooster[$idteller][3] . ":" . $u;
							if($klasrooster[$w][$idteller][3]==$u) {  // als uur van les uit rooster overeenkomt met plaats in tabel
								// echo $d . ":" . $klasrooster[$idteller][3] . ":" . $u;
								if ($data[$w][$d+5]>0) {  //  als er lesvrije of toetsvrije dagen zijn
									 if ($data[$w][$d+5]==1) { 
									 		echo "<td valign=\"top\" align=\"center\"><font color=\"#808080\"><i>" . $klasrooster[$w][$idteller][4] . "</i></font></td>";
											if ($toetsvrij==0)  {
												 $toetsvrij = 1;
												 $boodschap = "* ". $dag[$d] . " in week " . $wk . " is een toetsvrije lesdag!";
											}
									 }
									 if ($data[$w][$d+5]==2) { 
									 		if ($u==2)  {
												 echo "<td valign=\"top\" align=\"center\"><i>vrij</i></td>";
											}else{
												 echo "<td valign=\"top\" align=\"center\"><i></i></td>";
											}
									 }
									 $idteller = $idteller + 1; 
								}else{	
								
								
									 if ($klasrooster[$w][$idteller][7]>0) {  // als er een toets gepland staat, gewicht aangeven
								 	 		$tekst = "<br>" . $klasrooster[$w][$idteller][7];
											if (!isset($klasrooster[$w][$idteller][8])) {  //  geen opmerking
									 			 echo "<td valign=\"top\" align=\"center\"><font size=\"3\"><b><u>" . $klasrooster[$w][$idteller][4] . "</u>" . $tekst . "</b></font></td>";
											}else{
												 echo "<td valign=\"top\" align=\"center\"><font size=\"3\"><b><u><a href=\"#\" class=\"tooltip\">" . $klasrooster[$w][$idteller][4] . "<span>" . $klasrooster[$w][$idteller][8] . "</span></a></u>" . $tekst . "</b></font></td>"; 
											}
									 }else{
									 		$tekst = "";
									 		echo "<td valign=\"top\" align=\"center\"><font size=\"2\">" . $klasrooster[$w][$idteller][4] . $tekst . "</font></td>";
									 }
								
									 $idteller = $idteller + 1;
								}
							}else{  // geen les in rooster, dus lege cel plaatsten
								echo "<td></td>";
							}
			
						}
						if ($d<5) { echo "<td>|</td>"; }
					}					
					echo "</tr>";
								
				}
				
				?>
				
			</font>
					
		</table>
		</fieldset>	
		
		<?php
		
		echo "<tr><br>";
		echo "<td colspan =\"25\" align=\"left\" height=\"15\"><i>&nbsp;&nbsp;&nbsp;Het rooster voor week&nbsp;" . $data[3][1] . "&nbsp;is nog een voorlopig rooster, deze kan dus nog wijzigen!</i></td><br>";
		//  echo $toetsvrij;
		if (!($toetsvrij=0))  {
			 echo "<td colspan =\"25\" align=\"left\" height=\"15\"><font color=\"#808080\"><i>&nbsp;&nbsp;&nbsp;" . $boodschap . "</i></font></td>";
		}
		echo "</tr>";
		?>

<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=7314590; 
var sc_invisible=1; 
var sc_security="977e4a34"; 
</script>

<script type="text/javascript"
src="http://www.statcounter.com/counter/counter.js"></script>
<noscript><div class="statcounter"><a title="godaddy web
stats"
href="http://statcounter.com/godaddy_website_tonight/"
target="_blank"><img class="statcounter"
src="http://c.statcounter.com/7314590/0/977e4a34/1/"
alt="godaddy web stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
		
</body>
</html>
