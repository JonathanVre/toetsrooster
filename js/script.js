(function() {
    window.msg = {
        show : function(text) {

            $(".message-balloon").animate({
                top : "125px",
                opacity : 1
            }, 250);

            msg.text(text);
        },
        text : function(msg) {
            $(".message-balloon").html(msg);
        },

        hide : function() {
            $(".message-balloon").animate({
                "top" : "0",
                "opacity" : 0
            }, 250);
        }
    }
})();

(function() {
    var hideMessageTimeout = null;

    window.handle = {
        save : function(url, data) {
            handle.run({
                "method" : "POST",
                "data" : data,
                "url" : url
            }, "Bezig met opslaan...", "Opgeslagen", "Mislukt");
        },

        delete : function(url, data) {
            handle.run({
                "method" : "POST",
                "data" : data,
                "url" : url
            }, "Bezig met verwijderen...", "Verwijderd", "Mislukt");
        },

        run : function(obj, start, success, failed) {
            var r = $.ajax(obj);
            msg.show(start);

            r.done(function(response) {
                console.log(response);
                if (response.indexOf("Fout") > -1) {
                    msg.text(response);
                } else {
                    msg.text("<i class='text-success'>" + success + "</i>");
                }
            });

            r.fail(function() {
                msg.text("<i class='text-failed'>" + failed + "</i>");
            });

            r.always(function() {
                clearTimeout(hideMessageTimeout);
               hideMessageTimeout = setTimeout(msg.hide, 3000);
            });
        }
    }
})();

(function() {
    window.overzicht = {
        save : function(form_id) {
            handle.save("/handle/save_toets/", $("#" + form_id).serialize());
        },

        delete : function(id) {
            handle.delete("/handle/delete_toets/", {id: id});
        }
    };

    window.toetsen = {
        add : function(data) {
            handle.save("/handle/add_toets/", data);
        }
    }
})();