<?php

class Session
{
    public static function start()
    {
        if (session_id() == "") {
            session_start();
        }
    }

    public static function fill($array)
    {
        self::start();

        foreach ($array as $key => $value) {
           self::set($key, $value);
        }
    }

    /**
     * @param $key
     * @return null | mixed
     */
    public static function get($key)
    {
        self::start();

        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }

    public static function set($key, $value)
    {
        self::start();

        $_SESSION[$key] = $value;
    }
}