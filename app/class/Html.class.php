<?php

class Html
{
    public static function header($title = "", $with_menu = true)
    {
        $html = file_get_contents("app/header.html.php");
        $html = str_replace("{title}", $title, $html);

        if (!$with_menu) {
            return $html;
        }

        ob_start();
        include 'app/menu.html.php';
        $menu = ob_get_clean();

        return $html . $menu;
    }

    public static function footer($show_toolbar = true)
    {
        ob_start();
        $show_toolbar = $show_toolbar;
        include "app/footer.html.php";
        $footer = ob_get_clean();

        return $footer;
    }

    public static function startFooter()
    {
        $content = self::footer();
        return substr($content, 0, strpos($content,"</body>"));
    }

    public static function endFooter()
    {
        return "</body>\n</html>";
    }
}