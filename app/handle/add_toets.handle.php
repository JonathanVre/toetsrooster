<?php
// controleer of er die week en/of die dag wel een toets opgegeven mag worden.
$week = Request::post("week");
$gewicht = Request::post("gewicht");
$klas = Request::post("klas");

$toetsen_in_week = SQL::select(
    "
      SELECT SUM(gewicht) as totaal_gewicht 
      FROM toetsopgaven 
      WHERE week = '$week' 
        AND klas = '$klas'
");

$max_gewicht =  $toetsen_in_week["totaal_gewicht"] + $gewicht > 15 ? true : false;

if ($max_gewicht) {
    echo "Fout: het maximale gewicht van toetsen is op deze dag of in deze week al bereikt.";
} else {
    // get post data en sla het op als nieuwe toets
    $valid = true;
    $columns = ["code", "klas", "vak", "gewicht", "les", "week", "weekid"];
    $values = [];

    foreach ($columns as $column) {
        if (Request::post($column, false) === false) {
            $valid = false;
        }

        $values[$column] = Request::post($column);
    }

    $values["datum"] = date("Y-m-d");
    $values["opmerking"] = "";
    $values["isdeleted"] = 0;

    if ($valid) {
        SQL::insert("toetsopgaven", $values);
    }
}


