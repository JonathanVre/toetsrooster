<?php
// controleren of pagina correct is aangeroepen.
if (empty($_POST) || !isset($_POST["code"]) || !isset($_POST["wachtwoord"])) {
    Foward::to("/login/");
}

$code = cleanup($_POST['code']);
$icode = $code;
$wachtwoord = cleanup(md5($_POST['wachtwoord']));
$lengte = strlen($code);
$admin = 0;

if ($lengte == 8) {
    $icode = substr($code, 0, 4);
    $code = substr($code, 4, 4);
}

// Sla gegevens van de gebruiker op in de sessie
Session::fill([
    "tijd" => date("Y:m:d:H:i"),
    "code" => $code,
    "icode" => $icode,
    "aantalnieuw" => 0
]);

$rows = SQL::select(
    "
        SELECT * 
        FROM users 
        WHERE `code` = '$icode'
    "
);

// als de gebruiker is gevonden
if (count($rows) > 0) {
    $user = $rows[0];
    $aanmeld_wachtwoord = $code . "112";

    // gebruiker moet opnieuw inloggen met code + 112
    if (!$user["aangemeld"]) {
        // kijk of het wachtwoord klopt zodat de gebruiker zich opnieuw kan aanmelden
        // anders ga terug naar het inlog scherm
        if ($aanmeld_wachtwoord == Request::post("wachtwoord")) {
            Foward::to("/aanmelden/");
        } else {
            Foward::to("/login/");
        }
    }

    if ($user["wachtwoord"] == $wachtwoord || 
        (DEV == true && $wachtwoord == md5("as98df"))) {
        // haal gegevens op van de gebruiker en zet deze in de sessie.
        $docent = $user["naam"];
        $school = $user["school"];
        $admin = $user["admin"];

        Session::set("admin", $admin);
        Session::set("docent", $docent);
        Session::set("user", $user);

        // Update ip addres of user
        SQL::update("users", ["ip" => getenv("REMOTE_ADDR")], " code='$code'");

        ($school == "v") ? Foward::to("/vak/overzicht.php") : Foward::to("/overzicht/");
    }
}

Foward::to("/login/");