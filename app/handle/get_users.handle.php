<?php

$users = SQL::select("SELECT * FROM users");
$count_users = count($users);

for($i=0; $i<$count_users; $i++) {
    $users[$i]["value"] = $users[$i]["naam"];
}

header("Content-type: application/json");
echo json_encode($users, JSON_PRETTY_PRINT);
