<?php

$mentorklas = Klas::getMentorKlas($code);
$klassen = Klas::getKlassen($code);
$aantalklassen = count($klassen);
$klas = "";

// probeer eerst te kijken of er een mentor klas is
// anders kies de eerste klas die gevonden wordt
// als er geen klas gevonden wordt: kies Mh1a
if ($mentorklas != "") {
    $klas = $mentorklas;
    $aantalklassen = 1;
} else {
    $klas = (isset($klassen[0])) ? $klassen[0]["klas"] : "Mh1a";
}

$klas = Session::get("klas") != "" ? Session::get("klas") : $klas;
$klas = Request::post("klas", $klas);

Session::set("klas", $klas);

// id ophalen van huidige week
$huidige_week = SQL::select("SELECT * FROM weken WHERE week = $week");
$wid = $huidige_week[0]["id"];
$weken = SQL::select("SELECT * FROM weken WHERE id >= $wid ORDER BY id");

$selected_week = Request::post("cweek", $week);
$selected_week_id = 0;

foreach ($weken as $w) {
    if ($w["week"] == $selected_week) {
        $selected_week_id = $w["id"];
    }
}

?>

<?= Html::header("Toetsrooster van&nbsp;" . ucfirst($klas)); ?>

<?php require "rooster.page.php"; ?>

<?= Html::footer(); ?>
	