<?php
// ophalen van door docent code opgegeven toetsen
$toetsen = Toets::toetsenPerKlas($code);
$klassen = array_keys($toetsen);
$aantal = count($toetsen);
?>

<?= Html::header("Toetsoverzicht van " . $docent); ?>

<div class="content overzicht">
    <?php if ($aantal == 0) : ?>
        <p>Er zijn nog geen toetsen opgegeven.</p>
    <?php endif; ?>

    <?php if ($aantal > 0) : ?>
        <?php foreach ($klassen as $klas) : ;?>
            <h1><?= $klas; ?></h1>

            <table class="table table-border table-striped">
                <tr>
                    <th class="toets">Toets</th>
                    <th class="date">Aangemaakt op</th>
                    <th class="info">Omschrijving</th>
                    <th class="actions"></th>
                </tr>
                <?php foreach ($toetsen[$klas] as $toets) : ?>
                    <tr id="toets-row-<?= $toets["id"]; ?>">
                        <td>
                            Week: <?= $toets["week"] . " " . $toets["vak"] . " " . $toets["les"] . "e les van de week"; ?>
                        </td>
                        <td><?= $toets["datum"]; ?></td>
                        <td>
                            <form id="toets-<?= $toets["id"]; ?>">
                                <input type="hidden" name="id" value="<?= $toets["id"]; ?>" />
                                <input type="text" name="opmerking" value="<?= $toets["opmerking"]; ?>" />
                            </form>
                        </td>
                        <td>
                            <div class="btn-group right">
                                <a href="javascript:;" class="btn save-btn" data-id="toets-<?= $toets["id"]; ?>">
                                    <i class="glyphicon glyphicon-floppy-disk"></i>
                                </a>
                                <a href="javascript:;" class="btn delete-btn" data-id="<?= $toets["id"]; ?>">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endforeach; ?>
    <?php endif; ?>
</div>

<?= Html::startFooter(); ?>
    <script>
        $(".save-btn").each(function(index, value) {
            $(this).on("click", function() {
                var id = $(value).data("id");
                overzicht.save(id);
            })
        });

        $(".delete-btn").each(function(index, value) {
            $(this).on("click", function() {
                var id = $(value).data("id");
                overzicht.delete(id);

                $("#toets-row-" + id).remove();
            });
        });
    </script>
<?= Html::endFooter(); ?>
