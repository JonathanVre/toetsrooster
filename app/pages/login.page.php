<?= Html::header("Inloggen",false); ?>

    <div id="login-box">
        <h2>Toetsrooster voor mavo en lyceum
            <br><small><i>onderbouw</i></small>
        </h2>

        <form action="/handle/login/" method="post">
            <h3>Inloggen <?= (DEV) ? "<b><i>(test)</i></b>" : ""; ?></h3>
            <table class="table">
                <tr>
                    <th>Roostercode</th>
                    <td>
                        <input type="text" name="code" size="29" maxlength="8" value="<?= Session::get("code"); ?>"/>
                    </td>
                </tr>
                <tr>
                    <th>Wachtwoord</th>
                    <td>
                        <input type="password" name="wachtwoord" size="29" maxlength="20">
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <div class="btn-group right">
                            <input type="submit" name="submit" value="Inloggen" class="btn" />
                            <input type="reset" name="reset" value="Leegmaken" class="btn" />
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

<?= Html::footer(false); ?>