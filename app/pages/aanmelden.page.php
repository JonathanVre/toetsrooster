
<?= Html::header("Toetsrooster - Aanmelden", false); ?>

    <div id="login-box">
        <h2>Toetsrooster voor mavo en lyceum
            <br><small><i>onder- en bovenbouw</i></small>
        </h2>

        <form action="/handle/aanmelden/" method="POST">
            <h3>Aanmelden</h3>
            <table class="table">
                <tr>
                    <th>Roostercode</th>
                    <td>
                        <input type="text" name="code"  size="29" maxlength="6" value="<?= Session::get("code"); ?>"/>
                    </td>
                </tr>
                <tr>
                    <th>Wachtwoord</th>
                    <td>
                        <input type="password" name="wachtwoord" size="29" maxlength="20"/>
                    </td>
                </tr>
                <tr>
                    <th>Wachtwoord (controle)</th>
                    <td>
                        <input type="password" name="wachtwoord_controle" size="29" maxlength="20"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <div class="btn-group right">
                            <input type="submit" name="submit" value="Aanmelden" class="btn" />
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

<?= Html::endFooter(); ?>
