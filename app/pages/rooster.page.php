<?php
$can_edit = isset($can_edit) ? $can_edit : false;
$per_klas = $klas != "" ? true : false;
$disable_week = isset($disable_week) ? $disable_week : false;

// ophalen van het toetsrooster van de klas voor de 4 eerstvolgende weken
// er moet een geldig rooster van deze week in de tabel staan
$rooster = $per_klas ? Rooster::getByKlas(42, $klas) : Rooster::getByDocent(42, $code);

// opgegeven toetsen ophalen
if ($per_klas) {
    $toetsen = Toets::toetsenPerWeek($selected_week_id, $selected_week_id + 3, $klas);
} else {
    $toetsen = Toets::toetsenPerWeek($selected_week_id, $selected_week_id + 3, "", $code);
}

// weekinfo ophalen
$days = Rooster::$days;
$day_keys = Rooster::$day_keys;
$lessen_per_day = 9;
$boodschap = "";

// get full rooster with toetsen in it
$active_weken = SQL::select("SELECT * FROM weken WHERE id >= $selected_week_id ORDER BY id LIMIT 0,4");
$volledig_rooster = Rooster::sortVolledigRooster($rooster, $active_weken, 4, $toetsen);

$les_van_week = [];
$max_week = 15;
$max_day = 5;
$total_week = [];
$total_day = [];

if ($can_edit) {
    for ($w=0; $w<4; $w++) {
        $total_week[$w] = 0;
        for ($d=0; $d<count($days); $d++) {
            $total_day[$w][$d] = 0;

            for ($u=0; $u<9; $u++) {
                if (isset($volledig_rooster[$w][$d][$u]["toets"])) {
                    $total_week[$w] += $volledig_rooster[$w][$d][$u]["toets"]["gewicht"];
                    $total_day[$w][$d] += $volledig_rooster[$w][$d][$u]["toets"]["gewicht"];
                }
            }
        }
    }
}
?>

<div class="content">
    <form method="post">
        <?php if ($per_klas) : ?>
            <div class="input-group left">
                <div class="left sm-fixed">
                    <label class="control-label" for="klas">Klas</label>
                </div>

                <div class="input left md-fixed">
                    <select name="klas" id="klas" onchange="this.form.submit()">
                        <?php foreach ($klassen as $klas_option) : ?>
                            <option <?= trim(strtolower($klas_option["klas"])) == trim(strtolower($klas)) ? "selected" : ""; ?>>
                                <?= ucfirst($klas_option["klas"]); ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!$disable_week) : ?>
            <div class="input-group left">
                <div class="left sm-fixed">
                    <label for="cweek" class="control-label" for="cweek">Week</label>
                </div>

                <div class="input left md-fixed">
                    <select name="cweek" id="cweek" onchange="this.form.submit()">
                        <?php foreach ($weken as $week) : ?>
                            <option <?= $week["week"] == $selected_week ? "selected" : ""; ?>>
                                <?= $week["week"]; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        <?php endif; ?>
    </form>
    <br>
</div>

<div class="content rooster">
    <table class="table table-border table-rooster table-hover">
        <thead>
        <tr>
            <th></th>
            <?php foreach ($days as $day) : ?>
                <th colspan="<?= $lessen_per_day + 1; ?>">
                    <h3><?= $day; ?></h3>
                </th>
            <?php endforeach; ?>
        </tr>
        <tr>
            <td></td>
            <?php foreach ($days as $day) : ?>
                <td class="border-left"></td>
                <?php for($l=0; $l<$lessen_per_day; $l++) :?>
                    <td><?= $l + 1; ?></td>
                <?php endfor; ?>
            <?php endforeach; ?>
        </tr>
        </thead>

        <tbody>
        <?php for ($w=0; $w<count($active_weken); $w++) : ?>
            <tr>
                <td>
                    <b><?= $active_weken[$w]["week"]; ?></b> <br/> <br/>
                    <a href="https://www.hetstedelijkzutphen.nl/agenda/lijst/?tribe-bar-date=<?= ($active_weken[$w]["datum"]); ?>" target="_new">
                        <?= date("d/m", strtotime($active_weken[$w]["datum"])); ?>
                    </a>
                </td>
                <?php for ($d=0; $d<count($days); $d++) : ?>
                    <?php if ($active_weken[$w][$day_keys[$d]] == 1) {
                        $boodschap .= $days[$d] . " in week " . $active_weken[$w]["week"] . " is een toetsvrije lesdag! <br/>";
                    }
                    ?>

                    <?php if ($active_weken[$w][$day_keys[$d]] == 2) : ?>
                        <td colspan="10" class="les-vrij">
                            vrij <?= ($active_weken[$w]["docentopmerking"]) != "" ? " - " . $active_weken[$w]["docentopmerking"] : ""; ?>
                        </td>
                    <?php else : ?>

                        <td class="border-left"></td>
                        <?php for ($u=0; $u<$lessen_per_day; $u++) : ?>
                            <?php
                            if (isset($volledig_rooster[$w][$d][$u]["rooster"])) {
                                $vak = $volledig_rooster[$w][$d][$u]["rooster"]["vak"];
                                $les_van_week[$w][$vak] = isset($les_van_week[$w][$vak]) ? $les_van_week[$w][$vak] + 1 : 1;
                            }
                            ?>
                            <?php if ($per_klas) : ?>
                                <!-- Toon het rooster per klas -->
                                <?php if (isset($volledig_rooster[$w][$d][$u]["toets"])) : ?>
                                    <td class="toets">
                                        <b>
                                            <?= $volledig_rooster[$w][$d][$u]["toets"]["vak"] . "-" . $volledig_rooster[$w][$d][$u]["toets"]["gewicht"]; ?>

                                            <?php if ($volledig_rooster[$w][$d][$u]["toets"]["opmerking"] != "") : ?>
                                                <span class="toets-tooltip">
                                                Vak: <?= $volledig_rooster[$w][$d][$u]["toets"]["vak"]; ?> <br/>
                                                    <?= $volledig_rooster[$w][$d][$u]["toets"]["opmerking"]; ?>
                                            </span>
                                            <?php endif; ?>
                                        </b>
                                        <?= str_replace($volledig_rooster[$w][$d][$u]["toets"]["vak"], "", $volledig_rooster[$w][$d][$u]["rooster"]["vak"]); ?>
                                    </td>
                                
                                <?php elseif (isset($volledig_rooster[$w][$d][$u]["rooster"])) : ?>
                                    <td>
                                        <?php 
                                        $toets_select = "";
                                        // controleer of er voor deze les toetsen mogen worden opgegeven
                                        if ($can_edit && $total_week[$w] < $max_week && $total_day[$w][$d] < $max_day) {
                                            $toets_select_vak = "";

                                            // controleer of er meerdere vakken zijn op dit uur
                                            // zo ja, kijk per vak of hier een toets voor opgegeven kan worden
                                            if (strpos($vak, "<br/>") !== false) {
                                                $all_vakken = explode("<br/>", $vak);
                                                foreach ($all_vakken as $v) {
                                                    if (in_array($v, $vakken)) {
                                                        $toets_select_vak = $v;
                                                    }
                                                }
                                            } else if (in_array($vak, $vakken)){
                                                $toets_select_vak = $vak;
                                            }

                                            // wanneer er een vak is gevonden die de docent geeft 
                                            // en hier een toets voor mag opgeven, maak dan de select
                                            if ($toets_select_vak != "") {
                                                $toets_select = "
                                                    " . $vak . "<br/>
                                                    <select class='new-toets'
                                                            data-code='" . $code . "'
                                                            data-klas='" . $klas . "'
                                                            data-vak='" . $toets_select_vak . "'
                                                            data-les='" . $les_van_week[$w][$vak] . "'
                                                            data-week='" . $active_weken[$w]['week'] . "'
                                                            data-weekid='" . $active_weken[$w]['id'] . "'
                                                            data-vakken='" . $vak . "'>
                                                        <option>kies</option>";
                                                        for ($g=0; ($g<($max_day - $total_day[$w][$d]) && $g<3); $g++) {
                                                            $toets_select .= "<option>" . ($g + 1) . "</option>";
                                                        }
                                                        $toets_select .= "
                                                    </select>
                                                ";
                                            }
                                        }
                                        ?>
                                        
                                        <?= $toets_select != "" ? $toets_select : $vak; ?>
                                    </td>
                                <?php else : ?>
                                    <td></td>
                                <?php endif; ?>
                            <?php elseif (!$per_klas) : ?>
                                <!-- Toon het rooster voor de docent -->
                                <?php if (isset($volledig_rooster[$w][$d][$u]["toets"])) : ?>
                                    <td class="toets">
                                        <b>
                                            <?= $volledig_rooster[$w][$d][$u]["toets"]["klas"]; ?> <br/>
                                            <?= $volledig_rooster[$w][$d][$u]["toets"]["vak"] . "-" . $volledig_rooster[$w][$d][$u]["toets"]["gewicht"]; ?>
                                        </b>
                                    </td>
                                <?php elseif (isset($volledig_rooster[$w][$d][$u]["rooster"])) : ?>
                                    <td>
                                        <?= $volledig_rooster[$w][$d][$u]["rooster"]["klas"]; ?>
                                    </td>
                                <?php else: ?>
                                    <td></td>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endfor; ?>
                    <?php endif; ?>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
        </tbody>
    </table>
    <p><?= $boodschap; ?></p>
</div>